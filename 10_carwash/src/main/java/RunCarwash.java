import be.kdg.carwash.Car;
import be.kdg.carwash.Carwash;

/**
 * Test hiermee de klassen Car en Carwash uit.
 */
public class RunCarwash {
    public static void main(String[] args) {
        Carwash carwash = new Carwash();
        Car[] cars = {
                new Car(1, 0, carwash),
                new Car(2, 1, carwash),
                new Car(3, 3, carwash),
                new Car(4, 7, carwash),
                new Car(5, 8, carwash),
                new Car(6, 9, carwash)
        };
        for (Car car : cars) {
            new Thread(car).start();
        }
    }
}

/*
 Verwachte afdruk:

 Wagen nr 1 komt aan.
 Start wagen nr 1
 Wagen nr 2 komt aan.
 Wagen nr 2 moet wachten!
 Wagen nr 1 klaar
 Start wagen nr 2
 Wagen nr 3 komt aan.
 Wagen nr 3 moet wachten!
 Wagen nr 2 klaar
 Start wagen nr 3
 Wagen nr 3 klaar
 Wagen nr 4 komt aan.
 Start wagen nr 4
 Wagen nr 5 komt aan.
 Wagen nr 5 moet wachten!
 Wagen nr 4 klaar
 Start wagen nr 5
 Wagen nr 6 komt aan.
 Wagen nr 6 moet wachten!
 Wagen nr 5 klaar
 Start wagen nr 6
 Wagen nr 6 klaar
*/