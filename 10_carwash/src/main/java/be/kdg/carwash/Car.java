package be.kdg.carwash;

public class Car implements Runnable {
    private int wagenNummer;
    private int aankomstTijd;
    private Carwash carwash;

    public Car(int wagenNr, int aankomstTijd, Carwash carwash) {
        wagenNummer = wagenNr;
        this.aankomstTijd = aankomstTijd;
        this.carwash = carwash;
    }

    /**
     * Vul de run-methode van de klasse Car aan. In deze methode begin je met sleep(1000 *
     * aankomsttijd) om de verschillende aankomstmomenten te simuleren. Vervolgens druk
     * je af welke wagen aankomt (wagenNummer) en roep je de methode aankomstWagen op.
     * Daarna druk je af welke wagen nu aan de beurt is om gewassen te worden en voer je daarna
     * een sleep(1600) uit om de tijd nodig voor het wassen te simuleren.
     * Daarna kan de wagen vertrekken
     */
    public void run() {
            //TODO: werk uit

    }
}
