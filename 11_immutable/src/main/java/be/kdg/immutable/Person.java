package be.kdg.immutable;

import java.util.List;

public class Person {
    private String name;
    private int age;
    private Address address;
    private List<String> friends;

    public Person(String name, int age, Address address, List<String> friends) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.friends = friends;
    }

    public Address getAddress() {
        return address;
    }

    public List<String> getFriends() {
        return friends;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setFriends(List<String> friends) {
        this.friends = friends;
    }

    @Override
    public String toString() {
        return String.format("%s (age %d)\n\tAdress: %s\n\tFriends: %s",
                name, age, address, friends);
    }
}
