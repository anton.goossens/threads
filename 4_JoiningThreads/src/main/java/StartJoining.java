import be.kdg.join.StringThread;

import java.util.List;

public class StartJoining {
    public static void main(String[] args) {
        String[] strings = {"Alfa", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot", "Golf", "Hotel"};
        Thread[] threads = new Thread[8];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new StringThread(strings[i]);
            threads[i].start();
        }
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                // ...
            }
        }
        System.out.println("Alle threads ten einde");
        System.out.println(StringThread.getStringList());
    }
}

