import be.kdg.race.Racer;

public class StartRace {
    public static void main(String[] args) {
        Racer racerEen = new Racer("Peter");
        Racer racerTwee = new Racer("Julie");

        racerEen.start();
        racerTwee.start();

        // Kortere schrijfwijze:  new Racer("Peter").start();
    }
}
